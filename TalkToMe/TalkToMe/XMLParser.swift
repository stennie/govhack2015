//
//  XMLParser.swift
//  TalkToMe
//
//  Created by Andres Kievsky on 5/07/2015.
//  Copyright (c) 2015 bacon. All rights reserved.
//

import Foundation

class XMLParser : NSObject, NSXMLParserDelegate {

    var parser : NSXMLParser?
    var inItem : Bool = false
    var itemName : String? = nil
    var value : String? = nil
    var feature : Feature? = nil
    var parsedFeatures : Array<Feature> = []

    func parse(data : NSData) -> [Feature] {
        parser = NSXMLParser(data: data)
        parsedFeatures = []
        if let parser = parser {
            parser.delegate = self
            parser.parse()
        }
        return parsedFeatures
    }

    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [NSObject : AnyObject]) {
        if inItem {
//            println(elementName)
            itemName = elementName
        }
        if elementName == "item" {
            inItem = true
        }
    }

    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if inItem {
            if itemName != nil {
                var key = itemName ?? ""
                key = key.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())

                var val : String = value ?? ""
                val = val.stringByReplacingOccurrencesOfString("\n", withString: " ", options: NSStringCompareOptions.LiteralSearch, range: nil)
                val = val.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                val = val.stringByReplacingOccurrencesOfString("<br />", withString: "\n", options: NSStringCompareOptions.LiteralSearch, range: nil)
                val = val.stringByReplacingOccurrencesOfString("<br>", withString: "\n", options: NSStringCompareOptions.LiteralSearch, range: nil)
                val = val.stringByReplacingOccurrencesOfString("<strong>", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                val = val.stringByReplacingOccurrencesOfString("</strong>", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                if feature == nil {
                    feature = Feature()
                    feature!.category = nil
                    feature!.level = nil
                    feature!.size = "0 ha."
                }
                switch key {
                    case "description":
                        feature!.description = val
                        var descDict = Feature.parseDescription(val)
                        feature!.location = descDict["Location"]?.lowercaseString
                        feature!.title = descDict["Location"]?.lowercaseString
                        feature!.status = descDict["Status"]?.lowercaseString
                        feature!.type = descDict["Type"]?.lowercaseString
                        if descDict["Latitude"] != nil && descDict["Longitude"] != nil {
                            feature!.coordinates = ((descDict["Latitude"]! as NSString).doubleValue, (descDict["Longitude"]! as NSString).doubleValue)
                        }
                        if descDict["ALERT LEVEL"] != nil {
                            feature!.level = descDict["ALERT LEVEL"]!
                        }
                        if descDict["Alert level"] != nil {
                            feature!.level = descDict["Alert level"]!
                        }
                        if descDict["LOCATION"] != nil {
                            feature!.location = feature!.location ?? descDict["LOCATION"]!
                            feature!.title = feature!.title ?? descDict["LOCATION"]!
                        }
                        feature!.status = feature!.status ?? descDict["STATUS"]
                        feature!.status = feature!.status ?? descDict["Current Status"]
//                        feature!.det = feature!.details ?? descDict["Details"]
                        if descDict["TYPE"] != nil {
                            feature!.type = feature!.type ?? descDict["TYPE"]?.lowercaseString
                        }
                        if descDict["SIZE"] != nil {
                            feature!.size = feature!.size ?? descDict["SIZE"]?.lowercaseString
                    }
//
//                        println(descDict)
                    case "pubDate":
                        feature!.pubDate = val
                    case "georss:point":
                        var parts = val.componentsSeparatedByString(" ")

                    feature!.coordinates = ((parts[0] as NSString).doubleValue, (parts[1] as NSString).doubleValue)
                default:
                    println("")
                }
                println("<<< \(key): \(val)")
            }

            itemName = nil
        }
        if elementName == "item" {
            // emit


            if let f = feature {
                if f.type == nil {
                    f.type = "alert"
                }
                parsedFeatures.append(f)
            }

            feature = nil

            inItem = false
        }
        value = nil
//        println("/\(elementName)")
    }

    func parser(parser: NSXMLParser, foundCharacters string: String?) {
        if string == nil {
            return
        }
        if value != nil {
            self.value = self.value! + string!
//            self.value!.append(string!)
        } else {
            value = string!
        }
//        println(string)
    }


}