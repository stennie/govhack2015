//
//  Feature.swift
//  TalkToMe
//
//  Created by Andres Kievsky on 4/07/2015.
//  Copyright (c) 2015 bacon. All rights reserved.
//

import Foundation
import CoreLocation

class Feature {
//    There is a $TYPE incident at $TITLE which is $DISTANCE from here. It is currently $STATUS affecting $SIZE.
//    There is a [Bush Fire] incident at [Kalinda Road Bar Point] which is [10km from here]. It is currently [out of control] affecting [10 hectares].

    var coordinates : (Double, Double)?
    var category : String?
    var description : String?
    var title : String?
    var pubDate : String?

    var level : String?
    var location : String?
    var status : String?
    var type : String?
    var size : String?

    func setFromDict(d : NSDictionary?) {
        if let d = d {
            var coords = d["geometry"]?["coordinates"]
            println(coords)
            if let coords = coords as? [Double] {
                coordinates = (coords[1], coords[0])
            } else {
                coordinates = nil
            }

            if let properties = d["properties"] as? Dictionary<String, AnyObject> {
                if let cat = properties["category"] as? String {
                    category = cat
                }
                if var desc = properties["description"] as? String {
                    desc = desc.stringByReplacingOccurrencesOfString("<br />", withString: ".\n", options: NSStringCompareOptions.LiteralSearch, range: nil)
                    description = desc
                }
                if var t = properties["title"] as? String {
                    t = t.stringByReplacingOccurrencesOfString(" Hwy", withString: " Highway", options: NSStringCompareOptions.LiteralSearch, range: nil)
                    t = t.stringByReplacingOccurrencesOfString(" Mwy", withString: " Motorway", options: NSStringCompareOptions.LiteralSearch, range: nil)
                    title = t
                }
                if let pd = properties["pubDate"] as? String {
                    pubDate = pd
                }
            }

//            if let s = d["SIZE"] as? String {
//                size = s
//            }

            var d = Dictionary<String, String>()
            if let lines = description?.componentsSeparatedByString("\n") {
                for line : String in lines {
                    let components = line.componentsSeparatedByString(":")
                    if count(components) > 1 {
                        var first = components[0]
                        var value = line.substringFromIndex(advance(line.startIndex, count(first)+1))
                        value = value.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                        d[first] = value
                    }
                }
                if let l = d["ALERT LEVEL"] {
                    level = l
                }
                if var l = d["LOCATION"] {
                    l = l.stringByReplacingOccurrencesOfString(" Hwy", withString: " Highway", options: NSStringCompareOptions.LiteralSearch, range: nil)
                    l = l.stringByReplacingOccurrencesOfString(" Mwy", withString: " Motorway", options: NSStringCompareOptions.LiteralSearch, range: nil)
                    location = l
                }
                if let s = d["STATUS"] {
                    status = s
                }
                if let s = d["SIZE"] {
                    size = s
                }
                if let t = d["TYPE"] {
                    type = t
                }
            }

        }

    }
    func instructions(lastLocation : (Double, Double)?) -> [String]? {
        if type != nil && title != nil && status != nil && size != nil {
            var distance = "."
            if var d = distanceTo(lastLocation) {
                d = round(d/1000)
                if d == 0 {
                    d = 1
                }
                let d2 : Int = Int(d)
                distance = " which is \(d2) km from here."
            }
            var affecting = ""
            if size != "0 ha." && size != "0 ha" {
                affecting = " affecting \(size!)"
            }
            var incidentType = "a \(type!) incident"
            if type == "incident" {
                incidentType = "an incident"
            }
            if type == "accident" {
                incidentType = "an accident"
            }
            if type == "alert" {
                incidentType = "an alert"
            }
            return ["There is \(incidentType)",
                "At \(title!)\(distance)",
                "It is currently \(status!)"]
        }
        return nil
    }

    func distanceTo(location : (Double, Double)?) -> Double? {
        if let location = location, let coords = coordinates {
            var loc1 : CLLocation = CLLocation(latitude: location.0, longitude: location.1)
            var loc2 : CLLocation = CLLocation(latitude: coords.0, longitude: coords.1)
            var d : Double = loc1.distanceFromLocation(loc2)
            return d
        } else {
            return nil
        }
    }

    static func parseDescription(description : String!) -> Dictionary<String, String> {
        if let description = description {
            var d = Dictionary<String, String>()
            let lines = description.componentsSeparatedByString("\n")

            for line : String in lines {
                let components = line.componentsSeparatedByString(":")
                if count(components) > 1 {
                    var first = components[0]
                    var value = line.substringFromIndex(advance(line.startIndex, count(first)+1))
                    value = value.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                    first = first.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                    d[first] = value
                }
            }
            return d
        } else {
            return [:]
        }
    }

}