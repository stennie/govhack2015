//
//  ViewController.swift
//  TalkToMe
//
//  Created by Andres Kievsky on 4/07/2015.
//  Copyright (c) 2015 bacon. All rights reserved.
//

import UIKit
import AVFoundation
import CoreLocation

enum OptionType {
    case WhenSwipeLeftGo
    case WhenSwipeRightGo
    case WhenSwipeDownGo
    case WhenSwipeUpGo
    case Pause
}
class Option {
    var name: String
    var type: OptionType?
    var target: String?
    var f: ((myPos : Int) -> ())?
    init(name: String, type: OptionType?, target: String?) {
        self.name = name
        self.type = type
        self.target = target
    }
    convenience init(name: String, f: ((myPos : Int) ->())) {
        self.init(name: name, type: nil, target: nil)
        self.f = f
    }
    convenience init(name: String) {
        self.init(name: name, type: nil, target: nil)
    }
}

class ViewController: UIViewController, AVSpeechSynthesizerDelegate, CLLocationManagerDelegate {
    var leftSwipe : UISwipeGestureRecognizer!
    var rightSwipe : UISwipeGestureRecognizer!
    var upSwipe : UISwipeGestureRecognizer!
    var downSwipe : UISwipeGestureRecognizer!
    var tapGesture : UITapGestureRecognizer!
    let speechSynth : AVSpeechSynthesizer = AVSpeechSynthesizer()

    var locationManager: CLLocationManager!
    var lastLocation : (Double, Double)? = nil

    var things : [AnyObject] = []
    var curPos = 0

    var moreFeatures : [Feature]?

    @IBOutlet weak var mainTextView: UITextView!

    func showAlerts(myPos : Int) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), { () -> () in
            let urlToRequest = "http://www.rfs.nsw.gov.au/feeds/majorIncidents.json"
            let url = NSURL(string: urlToRequest)
            let inputData : NSData? = NSData(contentsOfURL: url!)
            var result : Int? = nil

            if let root = ViewController.parseJSON(inputData) {
                var features = Array<Feature>()
                if let featureData = root["features"] as? [Dictionary<String, AnyObject>] {
                    for f in featureData {
                        var feature = Feature()
                        feature.setFromDict(f)
                        features.append(feature)
                    }
                }
                features += self.moreFeatures ?? []

                var lastPos = self.things.count
                var showNearOnly = false
                if let me = self.things[myPos] as? Option {
                    if me.name == "near_me" {
                        showNearOnly = true
                    }
                }
                if showNearOnly {
                    features.sort({
                        $0.distanceTo(self.lastLocation) < $1.distanceTo(self.lastLocation)
                    })
                    var sortedFeatures = features.filter({ var d = $0.distanceTo(self.lastLocation); return d != nil && d < 100000 })
                    if sortedFeatures.isEmpty {
                        self.things.append("There are no alerts nearby.")
                    } else {
                        var plural = sortedFeatures.count == 1 ? "alert" : "alerts"
                        var beplural = sortedFeatures.count == 1 ? "is" : "are"
                        self.things.append("There \(beplural) \(sortedFeatures.count) \(plural) nearby.")
                    }
                    features = sortedFeatures
                } else {
                    var plural = features.count == 1 ? "alert" : "alerts"
                    var beplural = features.count == 1 ? "is" : "are"
                    self.things.append("There \(beplural) \(features.count) \(plural).")

                    features = features.reverse()
                }
                for feature in features {
                    var d = feature.distanceTo(self.lastLocation)
                    if let instructions = feature.instructions(self.lastLocation) {
                        for s in instructions {
                            self.things.append(s)
                        }
                    }
                }

                if self.things.count != lastPos {
                    result = lastPos
                }
            } else {
                self.things.append("There was an error getting a feed.")
                self.things.append("Swipe up to start over.")
            }

            if let result = result {
                self.curPos = result
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.nextThing()
                })
            }
        })
    }

    func setArray() {
        things = [
            Option(name: "beginning", type: nil, target: nil),
            "Welcome to Talk To Me.",
            "Swipe left to hear alerts near you.",
            "Swipe right to hear all alerts.",
            "Swipe up at any point to start over.",
            "Tap to skip a sentence.",
            Option(name: "", type: OptionType.Pause, target: nil),

            Option(name: "choose_alerts_near", type: OptionType.WhenSwipeLeftGo, target: "alerts_near_me"),
            Option(name: "choose_alerts_all", type: OptionType.WhenSwipeRightGo, target: "alerts_all"),

            Option(name: "alerts_near_me"),
            "Incidents Near me.",
            Option(name: "near_me", f: showAlerts),
            Option(name: "", type: OptionType.Pause, target: nil),
            
            Option(name: "alerts_all"),
            "All alerts.",
            Option(name: "", f: showAlerts),
            Option(name: "", type: OptionType.Pause, target: nil),
            
        ]
    }

    func loadMoreIncidents() {
        self.moreFeatures = []
        loadURL("http://www.esa.act.gov.au/feeds/currentincidents.xml", andThen: { (data : NSData?) -> () in
            if let data = data {
                var xmlParser = XMLParser()
                self.moreFeatures! += xmlParser.parse(data)
            }
            self.loadURL("https://data.emergency.vic.gov.au/Show?pageId=getIncidentRSS", andThen: { (data : NSData?) -> () in
                if let data = data {
                    var xmlParser = XMLParser()
                    self.moreFeatures! += xmlParser.parse(data)
                }
                self.loadURL("http://www.fire.tas.gov.au/Show?pageId=colBushfireSummariesRss", andThen: { (data : NSData?) -> () in
                    if let data = data {
                        var xmlParser = XMLParser()
                        self.moreFeatures! += xmlParser.parse(data)
                    }
                    self.loadURL("https://ruralfire.qld.gov.au/bushfirealert/bushfireAlert.xml", andThen: { (data : NSData?) -> () in
                        if let data = data {
                            var xmlParser = XMLParser()
                            self.moreFeatures! += xmlParser.parse(data)
                        }
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.nextThing()
                        })
                    });
                });
            });
        });
    }

    func loadURL(urlString : String, andThen: (data : NSData?) -> ()) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), { () -> () in
            var url: NSURL = NSURL(string: urlString)!
            var request1: NSURLRequest = NSURLRequest(URL: url)
            var response: AutoreleasingUnsafeMutablePointer<NSURLResponse?> = nil
            var err: NSError?
            var data = NSURLConnection.sendSynchronousRequest(request1, returningResponse: response, error: nil)
            //        parser.
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), { () -> () in
                andThen(data: data)
                })
            //        var dataVal =  NSURLConnection.sendSynchronousRequest(request1, returningResponse: response, error: &err)!
            //        println(response)
            //        if var jsonResult: NSDictionary? = NSJSONSerialization.JSONObjectWithData(dataVal, options: NSJSONReadingOptions.MutableContainers, error: &err) as? NSDictionary? {
            //            println("Synchronous\(jsonResult)")
            //        } else {
            //            println("failed")
            //
            //        }
        })
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(false)

        setArray()

        loadMoreIncidents()
    }

    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        var locValue:CLLocationCoordinate2D = manager.location.coordinate
        self.lastLocation = (locValue.latitude, locValue.longitude)

    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()

        ////

        leftSwipe = UISwipeGestureRecognizer(target: self, action: Selector("respondToSwipeGesture:"))
        leftSwipe.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(leftSwipe)

        rightSwipe = UISwipeGestureRecognizer(target: self, action: Selector("respondToSwipeGesture:"))
        rightSwipe.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(rightSwipe)

        upSwipe = UISwipeGestureRecognizer(target: self, action: Selector("respondToSwipeGesture:"))
        upSwipe.direction = UISwipeGestureRecognizerDirection.Up
        self.view.addGestureRecognizer(upSwipe)

        downSwipe = UISwipeGestureRecognizer(target: self, action: Selector("respondToSwipeGesture:"))
        downSwipe.direction = UISwipeGestureRecognizerDirection.Down
        self.view.addGestureRecognizer(downSwipe)

        tapGesture = UITapGestureRecognizer(target: self, action: Selector("respondToTapGesture:"))
        self.view.addGestureRecognizer(tapGesture)

        ////

        mainTextView.text = ""
    }

    func respondToTapGesture(gesture: UITapGestureRecognizer) {
        if curPos < things.count {
            if let s = things[curPos] as? String {
                stopSpeaking()
                nextThing()
            }
        }
    }

    func respondToSwipeGesture(gesture: UIGestureRecognizer) {

        if let swipeGesture = gesture as? UISwipeGestureRecognizer {

            var wantedOptionType : OptionType? = nil

            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Right:
                wantedOptionType = OptionType.WhenSwipeRightGo
                println("Swiped right")
            case UISwipeGestureRecognizerDirection.Left:
                wantedOptionType = OptionType.WhenSwipeLeftGo
                println("Swiped left")
            case UISwipeGestureRecognizerDirection.Down:
                wantedOptionType = OptionType.WhenSwipeDownGo
                println("Swiped down")
            case UISwipeGestureRecognizerDirection.Up:
                wantedOptionType = OptionType.WhenSwipeUpGo
                println("Swiped up")
            default:
                break
            }
            if wantedOptionType == OptionType.WhenSwipeUpGo {
                self.jumpToOptionWithName("beginning")
                return
            }

            if let wantedOptionType = wantedOptionType {
                var reachedStart = false
                var optionToExecute : Option? = nil
                for i in curPos..<things.count {
                    if let thing = things[i] as? Option {
                        reachedStart = true
                        if thing.type == wantedOptionType {
                            optionToExecute = thing
                            break;
                        }
                    } else {
                        if reachedStart {
                            break
                        }
                    }
                }
                if let optionToExecute = optionToExecute {
                    if let optionName = optionToExecute.target {
                        self.jumpToOptionWithName(optionName)
                    }
                }
                println(optionToExecute)
            }
        }
    }

    func jumpToOptionWithName(name : String) {
        var pos = findOptionWithName(name)
        if let pos = pos {
            if let option = things[pos] as? Option {
                self.stopSpeaking()
                curPos = pos
                self.nextThing()
            }
        }
    }

    func findOptionWithName(name : String) -> Int? {
        for i in 0..<things.count {
            if let thing = things[i] as? Option {
                if thing.name == name {
                    return i
                }
            }
        }
        return nil
    }

    func nextThing() {
        if things.count <= curPos {
            return
        }
        var thing : AnyObject = things[curPos]
        if let s = thing as? String {
            curPos++
            speakThing(s)
        } else {
            if let option = thing as? Option {
                if let f = option.f {
                    var result = f(myPos: curPos)
                    return
                }
                if option.type == OptionType.Pause {
                    println("approached non-string - returning.")
                } else {
                    curPos++
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.nextThing()
                    })
                }
            }
        }
    }
    func speakThing(s : String) {
        let speechUtterance = AVSpeechUtterance(string: s)

        speechUtterance.rate = 0.15
        speechUtterance.pitchMultiplier = 1.0
        speechUtterance.volume = 1.0
        speechUtterance.voice = AVSpeechSynthesisVoice(language: "en-AU")
        speechSynth.delegate = self

        speechSynth.speakUtterance(speechUtterance)
    }

    static func parseJSON(inputData : NSData?) -> Dictionary<String, AnyObject>? {
        if let inputData = inputData {
            var error: NSError?
            var d: NSDictionary = NSJSONSerialization.JSONObjectWithData(inputData, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary

            if let root = d as? Dictionary<String, AnyObject> {
                return root
            }
        }
        return nil
    }

    func go() {
        let urlToRequest = "http://www.rfs.nsw.gov.au/feeds/majorIncidents.json"
        let url = NSURL(string: urlToRequest)
        let inputData = NSData(contentsOfURL: url!)

        //
        //
        //println(inputData)

        if let inputData = inputData {
            var error: NSError?
            var d: NSDictionary = NSJSONSerialization.JSONObjectWithData(inputData, options: NSJSONReadingOptions.MutableContainers, error: &error) as! NSDictionary

            if let root = d as? Dictionary<String, AnyObject> {

                if let features = root["features"] as? [AnyObject] {
                    for feature in features {
                        if let feature = feature as? Dictionary<String, AnyObject> {
                            //                    println(feature["properties"])
                            if let properties = feature["properties"] as? Dictionary<String, AnyObject> {
                                var description = properties["description"] as! String
                                //                        description = description.replaceOccurrencesOfString("<br />", withString: "\n", options: 0, range: NSRange(0, count(description)))
                                description = description.stringByReplacingOccurrencesOfString("<br />", withString: ".\n", options: NSStringCompareOptions.LiteralSearch, range: nil)
                                let speechUtterance = AVSpeechUtterance(string: description)

                                println(description)
                                speechUtterance.rate = 0.15
                                speechUtterance.pitchMultiplier = 1.0
                                speechUtterance.volume = 1.0
                                speechUtterance.voice = AVSpeechSynthesisVoice(language: "en-AU")

//                                speechSynthesizer.speakUtterance(speechUtterance)

                                //                        println(description)

                            }
                        }
                    }
                }
                //        if let features = root["features"] {
                //            //        println(features[0])
                //            println(features[0]["geometry"])
                //            let properties : Dictionary = features[0]["properties"]
                //            println(properties["description"])
                //            println(features[0]["type"])
                //        }
            }
            //    println(entry?["properties"])
            //    if let properties : Dictionary = entry?["properties"] {
            //
            //    }
        }
        
    }

//    func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, didStartSpeechUtterance utterance: AVSpeechUtterance!)
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, didFinishSpeechUtterance utterance: AVSpeechUtterance!) {
        self.nextThing()
    }
//    func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, didPauseSpeechUtterance utterance: AVSpeechUtterance!)
//    func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, didContinueSpeechUtterance utterance: AVSpeechUtterance!)
//    func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, didCancelSpeechUtterance utterance: AVSpeechUtterance!)

    func speechSynthesizer(synthesizer: AVSpeechSynthesizer!, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance!) {
        if let s = utterance.speechString {
            let text = s.substringWithRange(s.rangeFromNSRange(characterRange)!)
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                let attrText = NSMutableAttributedString(string: s)
                attrText.addAttribute(NSForegroundColorAttributeName, value: UIColor.whiteColor(), range: NSMakeRange(0, count(s)))
                attrText.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(40), range: NSMakeRange(0, count(s)))
                attrText.addAttribute(NSForegroundColorAttributeName, value: UIColor.blackColor(), range: characterRange)
                attrText.addAttribute(NSBackgroundColorAttributeName, value: UIColor.whiteColor(), range: characterRange)
//                [text addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:characterRange];

//                println(text)
                self.mainTextView.attributedText = attrText
            })
        }
    }

    func stopSpeaking() {
        self.speechSynth.stopSpeakingAtBoundary(AVSpeechBoundary.Immediate)
        self.mainTextView.text = ""
    }

}


extension String {
    func rangeFromNSRange(nsRange : NSRange) -> Range<String.Index>? {
        if let from = String.Index(self.utf16.startIndex + nsRange.location, within: self),
            let to = String.Index(self.utf16.startIndex + nsRange.location + nsRange.length, within: self) {
                return from ..< to
        }
        return nil
    }
}