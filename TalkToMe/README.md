# TalkToMe.app

**TalkToMe** is an iOS app created for [GovHack 2015](https://www.govhack.org/govhack-2015/) on the weekend of July 4 & 5, 2015.

### Background

Our goal was to look at novel ways to make essential information highly accessible through open data APIs. We decided to take a mobile-first approach and consider information that would be useful for mobile users that could also be relevant with the context of a user's current location.

Our target audiences for accessibility include:

 - blind or visually impaired
 - low levels of reading literacy
 - less tech savvy app users

This proof of concept implementation is for iOS, but the approach could be extended to other smartphone devices. For example, Android also has [accessibility APIs](https://developer.android.com/guide/topics/ui/accessibility/index.html).

### App Overview

**TalkToMe** consumes information from open data APIs and presents a minimal navigation interface for users to select content and listen to it via text-to-speech synthesis.

Users are presented with two interaction choices on the main application screen:

 - (swipe left) to hear news of interest close to their current location
 - (swipe right) to hear all available news

At any time they can also (swipe up) to return to the main application screen.

### Data Sets Used (so far)

 * [Rural fire service current incidents](
http://data.nsw.gov.au/data/dataset/nsw-rural-fire-service-current-incidents-feed)

### Technology Used

 * [Swift programming language](https://developer.apple.com/swift/)
 * [iOS 8 SDK](Accessibilityhttps://developer.apple.com/accessibility/ios/) - Accessibility features including VoiceOver and AVSpeechSynthesizer

### Credits

Created with love & caffeine by Team Bacon™:

 - [Stennie](https://twitter.com/stennie)
 - [Andres](https://twitter.com/anktastic)