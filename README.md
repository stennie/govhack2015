# The Bacon™ Grill

Projects created by Team Bacon™ for [GovHack 2015](https://www.govhack.org/govhack-2015/). Some or all projects will contain traces of bacon and/or nuts.

## Team Bacon™
 - [Stennie](https://twitter.com/stennie)
 - [Andres](https://twitter.com/anktastic)
 
## Projects

 - **[Talk To Me](https://bitbucket.org/stennie/govhack2015/src/c18be00357a2186e4eb105d4fa9c4da6a0eb6f74/TalkToMe/?at=master)**
 
**TalkToMe** is an iOS app that consumes information from open data APIs (in formats such as GeoRSS & GeoJSON) and presents a minimal navigation interface using text-to-speech synthesis for content playback. Content is summarised & filtered by relevance to the user's current location (within 100km), with the expected content being emergency alerts such as information on bush fires and other natural disasters.

The app was written in Swift using iOS8 accessibility APIs.

For a short video overview, see [Talk To Me](https://www.youtube.com/watch?v=AnMQoFvFpRs) (YouTube).


### How to run Talk To Me:

   - Check out the code from the git repository
   - Open the TalkToMe.xcodeproj project
   - Compile and Run the program
   - Once the program is running, make sure to allow it access to the device's location.
   - If running in the simulator, you can change the simulated location of the iOS device by going to Debug->Location->Custom Location... and entering latitude of -33.865 and longitude of 151.2094 for Sydney.
   - Please note that this program was tested and developed on iOS 8 and the iPhone 4S simulator.
